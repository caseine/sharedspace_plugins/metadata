<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_metadata
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @copyright 2017 onwards Mike Churchward (mike.churchward@poetopensource.org)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Metadata';

$string['errorcontextnotfound'] = 'Invalid context subplugin "{$a->contextname}" requested.';
$string['instancemetadata'] = '{$a->instancename} metadata';
$string['metadata'] = 'Metadata';
$string['metadatadata'] = 'Value';
$string['metadatadescription'] = 'Description';
$string['metadatafor'] = 'Instance metadata';
$string['metadataname'] = 'Name';
$string['metadatasaved'] = 'Metadata saved.';
$string['privacy:metadata'] = 'Metadata plugin only manages data and fields. Individual subplugins may store personal data.';
$string['subplugintype_metadatacontext'] = 'Data context plugin';
$string['subplugintype_metadatacontext_plural'] = 'Data context plugins';
$string['subplugintype_metadatafieldtype'] = 'Data fieldtype plugin';
$string['subplugintype_metadatafieldtype_plural'] = 'Data fieldtype plugins';
$string['select_meta_from'] = 'Import metadata from another module';
$string['select_meta_from_help'] = 'Import metadatas from another module. All values will be replaced by the ones of the selected module so
                             the values you have just entered by hand (and not saved yet) will be lost. '.
                            'You can always switch back to the original saved values of the activity by deleting your selection.';
$string['select_meta_from_warning'] = 'Warning: it will replace current values in the form';

$string['autoset'] = 'Autoset this field';
$string['autosetinstructions'] = 'Instructions';
$string['fieldautosetting'] = 'Automatic field setting';
$string['fieldautosetting_help'] = 'You can set this field to be automatically updated upon element copy / backup.
Accepted keywords are:<br>
<ul>
<li><b>SET &lt;value&gt;</b>: Set the field to the specified value instead of copying it.</li>
<li><b>INHERIT &lt;field&gt;</b>: If the field exists and is not empty, copy it. Else, set the field to the value of the specified field from the original element. &lt;field&gt; must be a valid database field from local_metadata table.</li>
</ul>';
$string['instructionsrequired'] = 'Instructions are required.';
$string['instructionsinvalid'] = 'These instructions are invalid.';
$string['instructionstoolong'] = 'Instructions are too long.';

$string['displayonsummary'] = 'Display by default on summary';
$string['displayonsummary_help'] = 'If supported and set to yes, this field will be displayed by default on the summary of the corresponding item, e.g. on module view page.';
$string['allmetadata'] = 'All metadata';
