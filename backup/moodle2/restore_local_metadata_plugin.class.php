<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 *
 * @package   local_metadata
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/** Database:
 *
 * local_metadata(id, instanceid, fieldid, data, dataformat).
 */
defined('MOODLE_INTERNAL') || die();
/**
 * Provides the information to restore metadata.
 */
class restore_local_metadata_plugin extends restore_local_plugin
{
    const TABLE = 'local_metadata';

    /**
     * Returns the format information to attach to module element.
     */
    protected function define_module_plugin_structure() {
        return array(new restore_path_element('module_metadata', $this->get_pathfor('module_metadata')));
    }

    /**
     * Returns the format information to attach to course element.
     */
    protected function define_course_plugin_structure() {
        return array(new restore_path_element('course_metadata', $this->get_pathfor('course_metadata')));
    }

    protected function process_new_inherited_metadata_fields($data, $newid, $contextlevel) {
        global $DB;
        // This is a query to check that we do the following processing only once per element.
        $sql = "SELECT MIN(f.id) as minid
                FROM {local_metadata_field} f
                JOIN {local_metadata} m ON m.fieldid = f.id
                WHERE m.instanceid = :instanceid";
        if ($DB->get_record_sql($sql, array('instanceid' => $data->instanceid))->minid == $data->fieldid) {
            // Manage fields with INHERIT autoset instructions that are not already set.
            $sql = "SELECT f.*
                    FROM {local_metadata_field} f
                    LEFT JOIN {local_metadata} m ON m.fieldid = f.id AND m.instanceid = :instanceid
                    WHERE m.fieldid IS NULL AND f.contextlevel = :contextlevel
                    AND f.autoset = 1 AND " . $DB->sql_like('f.autosetinstructions', ':pattern');
            $unsetinheritfields = $DB->get_records_sql($sql,
                    array('instanceid' => $data->instanceid, 'contextlevel' => $contextlevel, 'pattern' => 'INHERIT%'));
            foreach ($unsetinheritfields as $inheritfield) {
                $match = array();
                if (preg_match('#^INHERIT (.+)$#', $inheritfield->autosetinstructions, $match)) {
                    $fieldid = $inheritfield->id;
                    if (($id = $DB->get_field(self::TABLE, 'id', array('fieldid' => $fieldid, 'instanceid' => $newid))) === false) {
                        // We do this check to avoid errors for duplicate keys.
                        // Key duplication can happen in cases where courses have been deleted,
                        // but module metadata deletion did not trigger correctly,
                        // and restoring modules by re-using ids.
                        // This last point is a terrible design mistake by moodle core, and happens in course restore/import.

                        $DB->insert_record(self::TABLE, (object) array(
                                'instanceid' => $newid,
                                'fieldid' => $fieldid,
                                'data' => $data->{$match[1]}
                        ));
                    } else {
                        $DB->update_record(self::TABLE, (object) array(
                                'id' => $id,
                                'data' => $data->{$match[1]}
                        ));
                    }
                }
            }
        }
    }

    public function process_module_metadata($data) {
        global $DB;
        $data = (object)$data;

        $newmoduleid = $this->task->get_moduleid();

        $this->process_new_inherited_metadata_fields($data, $newmoduleid, CONTEXT_MODULE);

        $field = $DB->get_record('local_metadata_field', array('id' => $data->fieldid));

        if (!empty($field->autoset) && !empty($field->autosetinstructions)) {
            $match = array();
            if (preg_match('#^SET (.+)$#', $field->autosetinstructions, $match)) {
                // Autoset instructions to simply set the value.
                $data->data = $match[1];
            } else if (preg_match('#^INHERIT (.+)$#', $field->autosetinstructions, $match) && empty($data->data)) {
                // Autoset instructions to inherit the value from a field, as value is currently empty.
                $data->data = $data->{$match[1]};
            }
        }

        $data->instanceid = $newmoduleid;

        if (!$DB->record_exists(self::TABLE, array('fieldid' => $data->fieldid, 'instanceid' => $newmoduleid))) {
            // We do this check to avoid errors for duplicate keys.
            // Key duplication can happen in cases where we are restoring a module that does not exist in the database,
            // e.g. when restoring external or deleted modules.

            $DB->insert_record(self::TABLE, $data);
        }
    }

    public function process_course_metadata($data) {
        global $DB;
        $data = (object)$data;
        $courseid = $this->task->get_courseid();
        if ($record = $DB->get_record(self::TABLE, ['instanceid' => $courseid, 'fieldid' => $data->fieldid])) {
            $data->id = $record->id;
            $data->instanceid = $courseid;
            $DB->update_record(self::TABLE, $data);
        } else {
            $data->instanceid = $courseid;
            $DB->insert_record(self::TABLE, $data);
        }
    }
}
