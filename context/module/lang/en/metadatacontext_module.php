<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Metadata module context plugin language file.
 *
 * @package local_metadata
 * @subpackage metadatacontext_module
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @copyright 2017 onwards Mike Churchward (mike.churchward@poetopensource.org)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['contextname'] = 'module';
$string['contexttitle'] = 'Module';
$string['metadatatitle'] = 'Module metadata';
$string['metadatadisabled'] = 'Metadata for modules is currently disabled.';
$string['metadataenabled'] = 'Use metadata for modules';
$string['pluginname'] = 'Module metadata context';
$string['privacy:metadata'] = 'Data stored in the activity module context only.';

$string['selectmodulesandmetadatafields'] = 'Select modules and metadata fields';
$string['selectmetadatafields'] = 'Select metadata fields to set';
$string['selectmetadatafields_error'] = 'Please select at least one metadata field.';
$string['fillinmetadata'] = 'Fill in metadata';
$string['selectmodules'] = 'Select modules to apply metadata to';
$string['selectmodules_error'] = 'Please select at least one module.';
$string['editingforcms'] = 'Editing metadata for course modules';
$string['metadatachangespreviewmessage'] = 'The following changes are about to be applied to modules metadata:';
$string['modulename'] = 'Module name';
$string['unchanged'] = 'Unchanged';
$string['overwrite'] = 'Overwrite';
$string['bulkeditmetadata'] = 'Bulk edit module metadata';
$string['confirmandfinish'] = 'Confirm and finish';
