<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace metadatacontext_module;

use moodleform;

require('../../../../config.php');
global $CFG;
require($CFG->libdir . '/formslib.php');

class bulkedit_select_form extends moodleform {
    protected function definition() {
        global $OUTPUT, $COURSE;

        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 1);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('header', 'modulesheader', get_string('selectmodules', 'metadatacontext_module'));

        $cms = get_fast_modinfo($COURSE->id, -1)->cms;

        if (count($cms)) {
            $this->add_checkbox_controller(1);
            $group = [];
            $sectionid = -1;
            foreach ($cms as $cm) {
                if ($cm->section != $sectionid) {
                    $sectionid = $cm->section;
                    $group[] =& $mform->createElement('static', 'headersection' . $sectionid, '', '<h3>' . get_section_name($COURSE->id, $cm->sectionnum) . '</h3>');
                }
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, [ 'class' => 'activityicon' ]);
                $group[] =& $mform->createElement('advcheckbox', 'applytocm' . $cm->id, $icon . $cm->get_formatted_name(), '', [ 'group' => 1, 'class' => 'my-1' ]);
            }
        }
        $mform->addGroup($group, 'groupmodules', get_string('selectmodules', 'metadatacontext_module'), '<br>', false);

        $mform->addElement('header', 'metadataheader', get_string('selectmetadatafields', 'metadatacontext_module'));

        $dataoutput = new output\manage_data();

        $group = [];
        foreach ($dataoutput->data as $catid => $items) {
            $group[] =& $mform->createElement('static', 'headercategory' . $catid, '', '<h3>' . $items['categoryname'] . '</h3>');
            unset($items['categoryname']);

            foreach ($items as $item) {
                if ($item->field->visible != PROFILE_VISIBLE_NONE) {
                    $group[] =& $mform->createElement('advcheckbox', $item->inputname, format_string($item->field->name));
                }
            }
        }
        $mform->addGroup($group, 'groupmetadata', get_string('selectmetadatafields', 'metadatacontext_module'), '<br>', false);

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('cancel');
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('continue'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');

    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $modulesok = false;
        foreach ($data as $field => $value) {
            if (substr($field, 0, 9) == 'applytocm' && $value == 1) {
                $modulesok = true;
                break;
            }
        }
        if (!$modulesok) {
            $errors['groupmodules'] = get_string('selectmodules_error', 'metadatacontext_module');
        }

        $metadataok = false;
        foreach ($data as $field => $value) {
            if (substr($field, 0, 21) == 'local_metadata_field_' && $value == 1) {
                $metadataok = true;
                break;
            }
        }
        if (!$metadataok) {
            $errors['groupmetadata'] = get_string('selectmetadatafields_error', 'metadatacontext_module');
        }
        return $errors;
    }
}

class bulkedit_form extends moodleform {
    protected $selectedmetadata;
    protected $selectedcms;

    public function __construct($selectedmetadata, $selectedcms, $action = null) {
        $this->selectedmetadata = $selectedmetadata;
        $this->selectedcms = $selectedcms;
        parent::__construct($action);
    }

    protected function definition() {
        global $COURSE, $OUTPUT;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 2);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'metadatafields', json_encode($this->selectedmetadata));
        $mform->setType('metadatafields', PARAM_RAW);

        $mform->addElement('hidden', 'selectedcms', json_encode($this->selectedcms));
        $mform->setType('selectedcms', PARAM_RAW);

        $cms = get_fast_modinfo($COURSE->id, -1)->cms;
        $html = '';
        foreach ($this->selectedcms as $cmid) {
            if (isset($cms[$cmid])) {
                $cm = $cms[$cmid];
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, [ 'class' => 'activityicon' ]);
                $html .= '<div class="activity mb-1">' . $icon . $cm->get_formatted_name() . '</div>';
            }
        }
        $mform->addElement('static', null, get_string('editingforcms', 'metadatacontext_module'), $html);

        $dataoutput = new output\manage_data();

        $mform->addElement('header', 'headermetadata', get_string('fillinmetadata', 'metadatacontext_module'));
        foreach ($dataoutput->data as $catid => $items) {
            $categoryname = $items['categoryname'];
            $headerdone = false;
            unset($items['categoryname']);

            foreach ($items as $item) {
                if (in_array($item->inputname, $this->selectedmetadata)) {
                    if (!$headerdone) {
                        $mform->addElement('static', 'headercategory' . $catid, '', '<h3>' . $categoryname . '</h3>');
                        $headerdone = true;
                    }
                    $item->edit_field($mform);
                }
            }
        }

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('cancel', 'cancel', get_string('back'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('continue'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');
    }
}

class bulkedit_confirm_form extends moodleform {
    protected $metadata;
    protected $selectedcms;

    public function __construct($metadata, $selectedcms, $action = null) {
        $this->metadata = (array) $metadata;
        $this->selectedcms = $selectedcms;
        parent::__construct($action);
    }

    protected function definition() {
        global $OUTPUT, $DB, $COURSE;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 3);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'metadata', json_encode($this->metadata));
        $mform->setType('metadata', PARAM_RAW);

        $mform->addElement('hidden', 'selectedcms', json_encode($this->selectedcms));
        $mform->setType('selectedcms', PARAM_RAW);

        $mform->addElement('html', '<div class="mb-2">' . get_string('metadatachangespreviewmessage', 'metadatacontext_module') . '</div>');

        $table = new \html_table();
        $table->head = [
                get_string('modulename', 'metadatacontext_module'),
        ];
        $dataoutput = new output\manage_data();
        foreach ($dataoutput->data as $items) {
            unset($items['categoryname']);

            foreach ($items as $item) {
                if (isset($this->metadata[$item->inputname])) {
                    $table->head[] = format_string($item->field->name);
                }
            }
        }

        $cms = get_fast_modinfo($COURSE->id, -1)->cms;
        foreach ($this->selectedcms as $cmid) {
            if (isset($cms[$cmid])) {
                $cm = $cms[$cmid];
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, [ 'class' => 'activityicon' ]);
                $row = [ '<span class="activity">' . $icon . $cm->get_formatted_name() . '</span>' ];

                $dataoutput = new output\manage_data($cm);
                foreach ($dataoutput->data as $items) {
                    unset($items['categoryname']);
                    /**
                     * @var \local_metadata\fieldtype\metadata $item
                     */
                    foreach ($items as $item) {
                        if (isset($this->metadata[$item->inputname])) {
                            $oldvalue = $DB->get_field('local_metadata', 'data', [ 'fieldid' => $item->field->id, 'instanceid' => $cmid ]);
                            $oldvaluedisplay = $item->display_data();
                            $newvalue = $this->metadata[$item->inputname];
                            $item->data = $item->edit_save_data_preprocess(is_object($newvalue) ? (array) $newvalue : $newvalue, new \stdClass());
                            $newvaluedisplay = $item->display_data();
                            $info = '';
                            if ($oldvalue === false || $oldvalue === null) {
                                $info = '<b class="text-success mr-1">[' . get_string('new') . ']</b>';
                                $oldvaluedisplay = null;
                            } else if ($oldvaluedisplay == $newvaluedisplay) {
                                $info = '<b class="text-info mr-1">[' . get_string('unchanged', 'metadatacontext_module') . ']</b>';
                                $oldvaluedisplay = null;
                            } else {
                                $info = '<b class="text-warning mr-1">[' . get_string('overwrite', 'metadatacontext_module') . ']</b>';
                            }
                            if ($oldvaluedisplay === '') {
                                $oldvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                            }
                            if ($newvaluedisplay === '') {
                                $newvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                            }
                            $row[] = $info . ($oldvaluedisplay !== null ? $oldvaluedisplay . '<i class="fa fa-fw fa-caret-right"></i>' : '') . $newvaluedisplay;
                        }
                    }
                }
                $table->data[] = $row;
            }
        }

        $mform->addElement('html', \html_writer::table( $table ));

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('cancel', 'cancel', get_string('back'));
        $buttonarray[] = &$mform->createElement('submit', 'confirmbutton', get_string('confirm'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');
    }
}

$courseid = required_param('courseid', PARAM_INT);

$context = \context_course::instance($courseid);
$courseurl = $context->get_url();

require_login($courseid);
require_capability('moodle/course:manageactivities', $context);

global $PAGE, $OUTPUT;

$PAGE->set_url('/local/metadata/context/module/bulkedit.php', [ 'courseid' => $courseid ]);
$PAGE->set_context($context);
$PAGE->set_title(get_string('bulkeditmetadata', 'metadatacontext_module'));

function local_metadata_print_progress_and_content($content, $currentstep) {
    global $OUTPUT;
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('bulkeditmetadata', 'metadatacontext_module'));
    if ($currentstep !== null) {

        $steps = [ 'selectmodulesandmetadatafields', 'fillinmetadata', 'confirmandfinish' ];
        $stepdots = [];

        $dotsize = 10;

        foreach ($steps as $step => $steptitle) {
            $html = '<div style="width:' . $dotsize . 'px; display: grid; justify-content: center;">';
            $html .= '<div class="border border-dark rounded-circle' . ($currentstep >= $step ? ' bg-primary' : '') . '" style="justify-self: center; width:' . $dotsize . 'px; height:' . $dotsize . 'px;"></div>';
            $html .= '<div class="text-center" style="user-select: none; width: 8em">' . get_string($steptitle, 'metadatacontext_module') . '</div>';
            $html .= '</div>';
            $stepdots[] = $html;
        }

        $line = '<div class="border border-dark" style="margin-top: ' . ($dotsize/2 - 1) . 'px; flex-basis:20%; min-width: 8em; height:1px;"></div>';

        echo \html_writer::div(implode($line, $stepdots), 'd-flex justify-content-center align-items-start m-y-1');
    }
    echo $content;
    echo $OUTPUT->footer();
}

$step0data = null;
$step1data = null;

$step = optional_param('step', 0, PARAM_INT);

if ($step == 3 && !empty($_POST['metadata']) && !empty($_POST['selectedcms'])) {
    $metadata = json_decode($_POST['metadata']);
    $cmids = json_decode($_POST['selectedcms']);
    $confirmform = new bulkedit_confirm_form($metadata, $cmids, $PAGE->url);
    if ($confirmform->get_data() !== null) {
        confirm_sesskey();

        // Everything is fine, save the submitted metadata.
        $metadata = (object) $metadata;
        require_once(__DIR__ . '/../../lib.php');
        foreach ($cmids as $cmid) {
            $metadata->id = $cmid;
            if (has_capability('moodle/course:manageactivities', \context_module::instance($cmid))) {
                \local_metadata_save_data($metadata, CONTEXT_MODULE);
            }
        }

        $content = $OUTPUT->notification(get_string('success'), \core\output\notification::NOTIFY_SUCCESS);
        $content .= $OUTPUT->continue_button($courseurl);
        local_metadata_print_progress_and_content($content, null);
        die();
    } else {
        // The "Back" button has been pressed.
        // Go back to the previous step with previously filled values.
        $step = 1;
        $step1data = [];
        foreach ($metadata as $field => $value) {
            $step1data[$field] = $value;
        }
        foreach ($cmids as $cmid) {
            $step1data['applytocm' . $cmid] = 1;
        }
    }
}

if ($step == 2 && !empty($_POST['metadatafields']) && !empty($_POST['selectedcms'])) {
    $metadatafields = json_decode($_POST['metadatafields']);
    $cmids = json_decode($_POST['selectedcms']);
    $bulkeditform = new bulkedit_form($metadatafields, $cmids, $PAGE->url);
    if (($formdata = $bulkeditform->get_data()) !== null) {
        $metadata = [];
        foreach ($formdata as $field => $value) {
            if (substr($field, 0, 21) == 'local_metadata_field_') {
                $metadata[$field] = $value;
            }
        }
        $confirmform = new bulkedit_confirm_form($metadata, $cmids, $PAGE->url);
        local_metadata_print_progress_and_content($confirmform->render(), 2);
        die();
    } else if ($bulkeditform->is_cancelled()) {
        $step = 0;
        $step0data = [];
        foreach ($metadatafields as $field) {
            $step0data[$field] = 1;
        }
        foreach ($cmids as $cmid) {
            $step0data['applytocm' . $cmid] = 1;
        }
    } else {
        local_metadata_print_progress_and_content($bulkeditform->render(), 1);
        die();
    }
}

if ($step == 0 || $step == 1) {
    $selectform = new bulkedit_select_form($PAGE->url);
    if (($data = $selectform->get_data()) !== null || $step1data !== null) {
        $selectedmetadata = [];
        $cmids = [];
        if ($data !== null) {
            foreach ($data as $field => $value) {
                if (substr($field, 0, 21) == 'local_metadata_field_' && $value == 1) {
                    $selectedmetadata[] = $field;
                } else if (substr($field, 0, 9) == 'applytocm' && $value == 1) {
                    $cmids[] = substr($field, 9);
                }
            }
        } else {
            foreach ($step1data as $field => $value) {
                if (substr($field, 0, 21) == 'local_metadata_field_') {
                    $selectedmetadata[] = $field;
                } else if (substr($field, 0, 9) == 'applytocm') {
                    $cmids[] = substr($field, 9);
                }
            }
        }

        $bulkeditform = new bulkedit_form($selectedmetadata, $cmids, $PAGE->url);
        $bulkeditform->set_data($step1data);
        local_metadata_print_progress_and_content($bulkeditform->render(), 1);
        die();
    } else if ($selectform->is_cancelled()) {
        redirect($courseurl);
        die();
    } else {
        $selectform->set_data($step0data);
        local_metadata_print_progress_and_content($selectform->render(), 0);
        die();
    }
}

// We are in an inconsistent state.
redirect($courseurl);
die();
