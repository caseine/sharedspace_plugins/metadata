<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_metadata
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017, onwards Poet
 */

/**
 * Module settings form.
 *
 * @package local_metadata
 * @copyright  2017, onwards Poet
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metadatacontext_module\output;

defined('MOODLE_INTERNAL') || die();

class manage_data_form extends \local_metadata\output\manage_data_form {

    /**
     * Add an option to import metadata from another module,
     * and use the parent method to add all metadata fields.
     * @param \MoodleQuickForm $mform
     * @param array $data
     * @param int|null $instanceid If not specified, it will be retrieved from the 'id' element of $mform.
     */
    public static function add_metadata_fields($mform, $data, $instanceid = null) {
        global $PAGE, $OUTPUT;

        if ($instanceid === null) {
            $instanceid = $mform->getElement('id')->getValue();
        }

        // Add a field to import metadata from another module.
        $mform->addElement('header', 'header_metadata_import', get_string('select_meta_from', 'local_metadata'));
        $course = $PAGE->course;
        $modinfo = get_fast_modinfo($course);
        $selectionlist = array();
        foreach ($modinfo->get_cms() as $cm) {
            $selectionlist[$cm->id] = $cm->id . ' -- ' . format_string($cm->name);
        }
        unset($selectionlist[$instanceid]);
        $PAGE->requires->js_call_amd('local_metadata/loadmetadata', 'showMetaDataInLabels', array($instanceid));
        $PAGE->requires->string_for_js('noselection', 'form');

        $options = array(
                'multiple'=> true,
                'tags' => false,
                'showsuggestions' => (count($selectionlist) > 0),
                'placeholder' => 'id -- name'
        );
        $warningicon = $OUTPUT->pix_icon('warning', get_string('select_meta_from_warning', 'local_metadata'), 'local_metadata');
        $str = '<div class="d-flex">
                    <span>' . get_string('select_meta_from', 'local_metadata') . '</span>
                    <span class="text-warning mx-1">' .
                    $warningicon . '
                    </span>
                </div>';
        $mform->addElement('autocomplete', 'local_metadata_select_meta_from', $str, $selectionlist, $options);
        $mform->addHelpButton('local_metadata_select_meta_from', 'select_meta_from', 'local_metadata');

        // Call the parent method that adds all metadata fields.
        parent::add_metadata_fields($mform, $data);
    }
}