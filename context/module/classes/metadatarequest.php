<?php
define("AJAX_SCRIPT", true);
require_once(__DIR__ . '/../../../../../config.php');

$cmid = required_param('cmid', PARAM_INT);

require_login();
require_capability('moodle/course:manageactivities', context_module::instance($cmid));

global $DB;
$resultat = $DB->get_records_sql("SELECT f.shortname, f.datatype, m.data, m.dataformat
FROM {local_metadata} m
JOIN {local_metadata_field} f ON f.id = m.fieldid
WHERE instanceid = :cmid", array('cmid' => $cmid));

foreach ($resultat as &$record) {
    if ($record->datatype == 'datetime') {
        // Add details to easily fill in datetime in form inputs.
        $calendartype = \core_calendar\type_factory::get_calendar_instance();
        $currentdate = $calendartype->timestamp_to_date_array($record->data);
        $record->datevalues = array(
                'minute' => $currentdate['minutes'],
                'hour' => $currentdate['hours'],
                'day' => $currentdate['mday'],
                'month' => $currentdate['mon'],
                'year' => $currentdate['year']);
    }
}

echo json_encode($resultat);

die();