define(['jquery', 'core/templates', 'core/notification'], function ($, Templates, notification) {
    return {
        showMetaDataInLabels: function(currentCmid) {

            const EDITOR_FIELDTYPE = 'textarea';
            const AUTOCOMPLETE_FIELDTYPE = 'autocomplete';
            const DATE_FIELDTYPE = 'datetime';

            function setAutocompleteFieldValues($fitem, values) {
                var $select = $fitem.find('select');

                var context = {
                        items: values.map(function(value) {
                            return {
                                value: value,
                                label: $select.find('option[value="' + value + '"]').html() || value
                            };
                        }),
                        noSelectionString: M.util.get_string('noselection', 'form'),
                        selectionId: '',
                        multiple: true
                };
                Templates.render('core/form_autocomplete_selection_items', context).promise()
                // Template above exists only since Moodle 3.7 (or later) - fallback to another template if there is a problem.
                .catch(() => Templates.render('core/form_autocomplete_selection', context).promise())
                .then(function(html, js) {
                    // Add the new tags.
                    Templates.replaceNodeContents($fitem.find('.form-autocomplete-selection'), html, js);

                    // Deselect all the options of the select element
                    var $options = $select.find('option');
                    $options.prop('selected', false);
                    // We remove newly created custom tags from the suggestions list when they are deselected.
                    $options.filter('[data-iscustom]').remove();

                    // Add and select the newly selected options
                    values.forEach(function(value) {
                        var $option = $select.find('option[value="' + value + '"]');
                        if ($option.length == 0) {
                            // The option does not exist yet, create it.
                            $option = $('<option>').text(value).attr('value', value)
                            // We mark newly created custom options as we handle them differently if they are deselected.
                            .attr('data-iscustom', true);
                            $select.append($option);
                        }
                        $option.prop('selected', true); // Set this option as selected.
                    });
                })
                .catch(notification.exception);
            }

            var $select = $('#id_local_metadata_select_meta_from');
            if ($select.length === 0) {
                // The form was created but not rendered, do nothing.
                return;
            }
            var currentValue = $select.val()[0]; // Results in undefined if no selection yet.
            $select.change(function() {
                // Tweak this autocomplete field to allow only a single value.
                currentValue = $select.val().filter(e => e !== currentValue)[0];
                if (currentValue !== undefined) {
                    setAutocompleteFieldValues($('#fitem_id_local_metadata_select_meta_from'), [currentValue.trim()]);
                }

                var selectedModule = currentValue || currentCmid;

                $.ajax('/local/metadata/context/module/classes/metadatarequest.php?cmid=' + selectedModule)
                .done(function(response) {
                    for (var k in response) {
                        var metadataField = response[k];
                        var fieldName = 'local_metadata_field_' + metadataField.shortname;

                        var eltid = "id_" + fieldName;

                        if ($('#' + eltid).length && (document.getElementById(eltid).disabled || $('#' + eltid).find(":disabled").length)) {
                            // Do not set disabled fields.
                            continue;
                        }

                        if (metadataField.shortname == "Status" && currentCmid != selectedModule) {
                            // Set the metadata status to not ok.
                            metadataField.data = "Not Ok";
                        }

                        if (metadataField.datatype == AUTOCOMPLETE_FIELDTYPE) {
                            var values = metadataField.data !== '' ? metadataField.data.split(',').map(s => s.trim()) : [];
                            setAutocompleteFieldValues($('#fitem_' + eltid), values);
                        } else {
                            // More standard fields.
                            var $field;
                            if (metadataField.datatype == EDITOR_FIELDTYPE) {
                                // Atto editor: set both format and text.
                                $('[name=' + fieldName + '\\[format\\]]').val(metadataField.dataformat);
                                $('#id_' + fieldName + 'editable').html(metadataField.data);
                                $field = $('[name=' + fieldName + '\\[text\\]]');
                            } else if (metadataField.datatype == DATE_FIELDTYPE) {
                                // Date editor: set all fields.
                                if (metadataField.data > 0) {
                                    $('[name=' + fieldName + '\\[day\\]]').val(metadataField.datevalues.day);
                                    $('[name=' + fieldName + '\\[month\\]]').val(metadataField.datevalues.month);
                                    $('[name=' + fieldName + '\\[year\\]]').val(metadataField.datevalues.year);
                                    $('[name=' + fieldName + '\\[hour\\]]').val(metadataField.datevalues.hour);
                                    $('[name=' + fieldName + '\\[minute\\]]').val(metadataField.datevalues.minute);
                                    $('[name=' + fieldName + '\\[enabled\\]]').prop('checked', true);
                                } else {
                                    $('[name=' + fieldName + '\\[enabled\\]]').prop('checked', false);
                                }
                                M.form.updateFormState($('[name=' + fieldName + '\\[enabled\\]]').closest('form').attr('id'));
                                continue; // Datetime form control is special, no item has the simple input name.
                            } else {
                                // Standard field.
                                $field = $('[name=' + fieldName + ']');
                            }

                            // Set the data.
                            $field.val(metadataField.data);

                            // Some special cases.
                            if ($field.is('textarea')) { // Textareas need this to be actually updated.
                                $field.html(metadataField.data);
                            }
                            if ($field.is('input[type=checkbox]')) { // Checkboxes are special.
                                $field.prop('checked', metadataField.data == 1);
                            }
                        }
                    }
                })
                .fail(notification.exception);
            });
        }
    };
});