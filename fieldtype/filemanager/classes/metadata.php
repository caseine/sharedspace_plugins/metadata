<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Textarea profile field define.
 *
 * @package   profilefield_textarea
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metadatafieldtype_filemanager;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/repository/lib.php');

/**
 * Class local_metadata_field_filemanager.
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class metadata extends \local_metadata\fieldtype\metadata {

    /** @var int */
    public $mform_var;
    

    /**
     * Adds elements for this field type to the edit form.
     * @param moodleform $mform
     */
    public function edit_field_add($mform) {
        // Create the form field.
        $mform_var = $mform;
        $mform->addElement('filemanager', 'attachments', get_string('files'), null,
        array('subdirs' => 0, 'maxfiles' => 10,
              'accepted_types' => '*', 'return_types'=> FILE_INTERNAL | FILE_EXTERNAL));

    }

    /**
     * Overwrite base class method, data in this field type is potentially too large to be included in the user object.
     * @return bool
     */
    public function is_instance_object_data() {
        return true;//TODO change to false
    }

    /**
     * Process incoming data for the field.
     * @param stdClass $data
     * @param stdClass $datarecord
     * @return mixed|stdClass
     */
    /*public function edit_save_data_preprocess($data, $datarecord) {
        if ($draftitemid = file_get_submitted_draft_itemid('attachments')) {
            file_save_draft_area_files($draftitemid, $context->id, 'local_metadata', 'attachments', 0, array('subdirs' => false, 'maxfiles' => 10));
        }
        return $data;
    }*/

        /**
     * Load instance data for this metadata field, ready for editing.
     * @param stdClass $instance
     */
    /*public function edit_load_instance_data($instance) {
        if (empty($entry->id)) {
            $entry = new stdClass;
            $entry->id = null;
        }
         
        $draftitemid = file_get_submitted_draft_itemid('attachments');
         
        file_prepare_draft_area($draftitemid, $context->id, 'local_metadata', 'attachments', $entry->id,
                                array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 10));
         
        $entry->attachments = $draftitemid;
         
        $mform->set_data($entry);
    }*/

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return [PARAM_RAW, NULL_NOT_ALLOWED];
    }
}


