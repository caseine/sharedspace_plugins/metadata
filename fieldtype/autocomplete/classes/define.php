<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Autocomplete profile field definition.
 *
 * @package    profilefield_autocomplete
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}, 2022 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metadatafieldtype_autocomplete;

defined('MOODLE_INTERNAL') || die;

/**
 * Class local_metadata_define_autocomplete
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}, 2022 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class define extends \local_metadata\fieldtype\define_base {

    /**
     * Add elements for creating/editing a text profile field.
     * @param \MoodleQuickForm $form
     */
    public function define_form_specific($form) {
        // Default data.
        $form->addElement('text', 'defaultdata', get_string('defaultdata', 'metadatafieldtype_autocomplete'), 'size="50"');
        $form->setType('defaultdata', PARAM_TEXT);
        $form->addHelpButton('defaultdata', 'defaultdata', 'metadatafieldtype_autocomplete');

        // Param 1 for autocomplete type is whether it allows multiple values.
        $form->addElement('selectyesno', 'param1', get_string('multiple', 'metadatafieldtype_autocomplete'));
        $form->setDefault('param1', 1);
        $form->addHelpButton('param1', 'multiple', 'metadatafieldtype_autocomplete');

        // Param 2 for autocomplete type are the autocomplete options.
        $form->addElement('autocomplete', 'param2', get_string('options', 'metadatafieldtype_autocomplete'), array(),
                array('tags' => true, 'multiple' => true, 'showsuggestions' => false, 'placeholder' => get_string('options', 'metadatafieldtype_autocomplete')));
        $form->setType('param2', PARAM_TEXT);
        $form->addHelpButton('param2', 'options', 'metadatafieldtype_autocomplete');

        // Param 3 for autocomplete type is the placeholder example.
        $form->addElement('text', 'param3', get_string('placeholder', 'metadatafieldtype_autocomplete'));
        $form->setType('param3', PARAM_TEXT);
        $form->addHelpButton('param3', 'placeholder', 'metadatafieldtype_autocomplete');
    }

    public function define_after_data(&$form) {
        // This is Necessary because multiple values may have been added and interpreted as a single value - this fixes the problem.
        $autocomplete = $form->getElement('param2');
        $autocomplete->setValue($autocomplete->getValue());
    }

    public function define_save_preprocess($data) {
        $data->param2 = implode(',', $data->param2);
        return $data;
    }
}