<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Autocomplete profile field.
 *
 * @package    profilefield_autocomplete
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}, 2022 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metadatafieldtype_autocomplete;

defined('MOODLE_INTERNAL') || die;

/**
 * Class local_metadata_field_autocomplete
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}, 2022 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class metadata extends \local_metadata\fieldtype\metadata {

    /**
     * Add fields for editing an autocomplete profile field.
     * @param \MoodleQuickForm $mform
     */
    public function edit_field_add($mform) {
        // Text with autocomplete and placeholders
        $options = $this->get_autocomplete_options();
        $attributes = array(
                'multiple' => !isset($this->field->param1) || ($this->field->param1 != 0),
                'tags' => true,
                'showsuggestions' => (count($options) > 0),
                'placeholder' => $this->get_placeholder(),
        );
        $mform->addElement('autocomplete', $this->inputname, format_string($this->field->name), $options, $attributes);

        $mform->setType($this->inputname, PARAM_TEXT);
    }

    public function edit_field_set_default($mform) {
        // Necessary because setting both actual value and default value result in all these values being selected.
        if ($this->data === null && !empty($this->field->defaultdata)) {
            $mform->setDefault($this->inputname, $this->format_data($this->field->defaultdata));
        }
    }

    public function edit_load_instance_data($instance) {
        if ($this->data !== null) {
            $instance->{$this->inputname} = $this->format_data($this->data);
        }
    }

    protected function format_data($data) {
        $rawdata = explode(',', $data);
        $cleandata = array_filter(array_map('trim', $rawdata), 'strlen');
        return array_combine($cleandata, $cleandata); // Return an associative array.
    }

    public function edit_save_data_preprocess($data, $datarecord) {
        $selectedvalues = $data;
        $fieldvalue = '';
        if (isset($selectedvalues)  && !is_null($selectedvalues)) {
            if (is_string($selectedvalues)) {
                $fieldvalue = $data;
            } else if (count($selectedvalues) > 0) {
                $cleanvalues = array_filter(array_map('trim', $selectedvalues), 'strlen');
                $fieldvalue = implode(', ', $cleanvalues);
            }
        }
        return $fieldvalue;
    }

    protected function get_autocomplete_options() {
        $options = array();
        if (!empty($this->field->param2)) {
            $options = $this->format_data($this->field->param2);
        }
        return $options;
    }

    protected function get_placeholder() {
        $placeholder = '';
        if (!empty($this->field->param3)) {
            $values = explode(';', $this->field->param3);
            $placeholder = get_string('placeholdera', 'metadatafieldtype_autocomplete', $values[random_int(0, count($values) - 1)]);
        }
        return $placeholder;
    }

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return [PARAM_TEXT, NULL_NOT_ALLOWED];
    }
}
