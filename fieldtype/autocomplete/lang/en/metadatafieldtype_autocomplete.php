<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Metadata autocomplete fieldtype plugin language file.
 *
 * @package local_metadata
 * @subpackage metadatafieldtype_autocomplete
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @copyright 2017 onwards Mike Churchward (mike.churchward@poetopensource.org), 2022 Astor Bizard
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['pluginname'] = 'Autocomplete metadata fieldtype';
$string['displayname'] = 'Tags';

$string['defaultdata'] = 'Default value';
$string['defaultdata_help'] = 'Default value for the field, or comma-separated list of default values (if "Multiple values" is set to Yes).';
$string['multiple'] = 'Multiple values';
$string['multiple_help'] = 'Whether this field should accept more than one value at a time.';
$string['placeholder'] = 'Placeholder';
$string['placeholder_help'] = 'Specify a value that will be displayed as an example in the field.<br>
You can also specify a list of values separated by semicolons. One of these values will be randomly drawn and displayed.';
$string['placeholdera'] = 'Example: {$a}';
$string['options'] = 'Suggestions';
$string['options_help'] = 'Suggestions that will be provided to users filling this field.';