<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Sharing Cart
 *
 *  @package    local_metadata
 *  @copyright  2017 onwards Mike Churchward (mike.churchward@poetopensource.org) - extension for caseine
 *  @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 *  Local meta data upgrade
 *
 *  @global moodle_database $DB
 */
function xmldb_local_metadata_upgrade($oldversion = 0)
{
	global $DB;

	$dbman = $DB->get_manager();

    if ($oldversion < 2019092500) {

        // Define field defaultforsearch to be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('defaultforsearch', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'forceunique');

        // Conditionally launch add field defaultforsearch.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Metadata savepoint reached.
        //NICO+HAD: what does that do ? Florent ?
        //upgrade_plugin_savepoint(true, XXXXXXXXXX, 'local', 'metadata');
    }

    if ($oldversion < 2021020300) {

        // Define field requiredforsharing AND placeholdersto be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field1= new xmldb_field('requiredforsharing', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'defaultforsearch');
        // Conditionally launch add field requiredforsharing.
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }

        $field2 = new xmldb_field('placeholders', XMLDB_TYPE_TEXT, null, null, null, null, null, 'requiredforsharing');
        // Conditionally launch add field placeholders.
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
    }

    if ($oldversion < 2021042800) {

        // Define field to be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('representatives', XMLDB_TYPE_TEXT, null, null, null, null, null, 'placeholders');
        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2021042900) {

        // Define field to be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('param6', XMLDB_TYPE_TEXT, null, null, null, null, null, 'param5');
        // Conditionally launch add field.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2022061200) {

        $rs = $DB->get_recordset('local_metadata_field', array('datatype' => 'text'));
        foreach ($rs as $record) {
            if ($record->param6 === null || $record->param6 != 0) {
                // This is a multiple text input, change it to autocomplete.
                $record->datatype = 'autocomplete';
                $record->param1 = 1;
                $record->param2 = $record->representatives;
                $record->param3 = $record->placeholders;
                $record->param4 = null;
                $record->param5 = null;
                $DB->update_record('local_metadata_field', $record);
            }
        }
        $rs->close();

        $table = new xmldb_table('local_metadata_field');

        // Define field param6 to be dropped from local_metadata_field.
        $field = new xmldb_field('param6');

        // Conditionally launch drop field param6.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field placeholders to be dropped from local_metadata_field.
        $field = new xmldb_field('placeholders');

        // Conditionally launch drop field placeholders.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Define field representatives to be dropped from local_metadata_field.
        $field = new xmldb_field('representatives');

        // Conditionally launch drop field representatives.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }

        // Metadata savepoint reached.
        upgrade_plugin_savepoint(true, 2022061200, 'local', 'metadata');
    }

    if ($oldversion < 2022061201) {
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('defaultforcolumns', XMLDB_TYPE_INTEGER, '3', null, null, null, 1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2022070801) {

        // Define field autoset to be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('autoset', XMLDB_TYPE_INTEGER, '2', null, null, null, null, 'defaultforcolumns');

        // Conditionally launch add field autoset.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field autosetinstructions to be added to local_metadata_field.
        $field = new xmldb_field('autosetinstructions', XMLDB_TYPE_CHAR, '255', null, null, null, null, 'autoset');

        // Conditionally launch add field autosetinstructions.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Metadata savepoint reached.
        upgrade_plugin_savepoint(true, 2022070801, 'local', 'metadata');
    }

    if ($oldversion < 2024091700) {

        // Define field displayonsummary to be added to local_metadata_field.
        $table = new xmldb_table('local_metadata_field');
        $field = new xmldb_field('displayonsummary', XMLDB_TYPE_INTEGER, '2', null, null, null, '0', 'requiredforsharing');

        // Conditionally launch add field displayonsummary.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Metadata savepoint reached.
        upgrade_plugin_savepoint(true, 2024091700, 'local', 'metadata');
    }

	return true;
}
